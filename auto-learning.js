// ==UserScript==
// @name         auto-learning
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Auto Learning
// @author       Arc
// @match        http://rsedu.ch.mnr.gov.cn/*
// @grant        none
// @run-at       document-end
// ==/UserScript==

(function(){

    function sleep(delay)
    {
        var start = (new Date()).getTime();
        while((new Date()).getTime() - start < delay) {
            continue;
        }
    }

    if(localStorage['cehui_refresh']==1)
    {
        localStorage["cehui_refresh"]=0;
        location.reload(true);
    }

    var loc_ulr = window.location.href
    var gotoherf = 'http://rsedu.ch.mnr.gov.cn/'
    if (loc_ulr == 'http://rsedu.ch.mnr.gov.cn/')
    {window.localStorage.clear();
    return }

    if (loc_ulr =='http://rsedu.ch.mnr.gov.cn//index/course/addCorn')
    {
        localStorage['cehui_refresh']=1;
        localStorage['xuanxiu_refresh']= 1;
        gotoherf = 'http://rsedu.ch.mnr.gov.cn//index/user/myColumn?columnType=3'
        window.location.href = gotoherf
    }

    if (loc_ulr =='http://rsedu.ch.mnr.gov.cn//index/user/myColumn?columnType=3')
    {
        var hrefURL
        var allhref= new Array();
        var hrefArr = document.getElementsByTagName('a'); //获取这个页面的所有A标签
        for( var i=0; i<hrefArr.length; i++ )
        {
            hrefURL = hrefArr[i].href;
            if (hrefURL.indexOf('orderId') != -1){allhref.push(hrefURL)}
        }
        if (localStorage['http://rsedu.ch.mnr.gov.cn//index/user/myColumn?columnType=2&page=10']!='done'){gotoherf ='http://rsedu.ch.mnr.gov.cn//index/user/myColumn?columnType=2&page=10'}
        if (localStorage['http://rsedu.ch.mnr.gov.cn//index/user/myColumn?columnType=2&page=9']!='done'){gotoherf ='http://rsedu.ch.mnr.gov.cn//index/user/myColumn?columnType=2&page=9'}
        if (localStorage['http://rsedu.ch.mnr.gov.cn//index/user/myColumn?columnType=2&page=8']!='done'){gotoherf ='http://rsedu.ch.mnr.gov.cn//index/user/myColumn?columnType=2&page=8'}
        if (localStorage['http://rsedu.ch.mnr.gov.cn//index/user/myColumn?columnType=2&page=7']!='done'){gotoherf ='http://rsedu.ch.mnr.gov.cn//index/user/myColumn?columnType=2&page=7'}
        if (localStorage['http://rsedu.ch.mnr.gov.cn//index/user/myColumn?columnType=2&page=6']!='done'){gotoherf ='http://rsedu.ch.mnr.gov.cn//index/user/myColumn?columnType=2&page=6'}
        if (localStorage['http://rsedu.ch.mnr.gov.cn//index/user/myColumn?columnType=2&page=5']!='done'){gotoherf ='http://rsedu.ch.mnr.gov.cn//index/user/myColumn?columnType=2&page=5'}
        if (localStorage['http://rsedu.ch.mnr.gov.cn//index/user/myColumn?columnType=2&page=4']!='done'){gotoherf ='http://rsedu.ch.mnr.gov.cn//index/user/myColumn?columnType=2&page=4'}
        if (localStorage['http://rsedu.ch.mnr.gov.cn//index/user/myColumn?columnType=2&page=3']!='done'){gotoherf ='http://rsedu.ch.mnr.gov.cn//index/user/myColumn?columnType=2&page=3'}
        if (localStorage['http://rsedu.ch.mnr.gov.cn//index/user/myColumn?columnType=2&page=2']!='done'){gotoherf ='http://rsedu.ch.mnr.gov.cn//index/user/myColumn?columnType=2&page=2'}
        if (localStorage['http://rsedu.ch.mnr.gov.cn//index/user/myColumn?columnType=2&page=1']!='done'){gotoherf ='http://rsedu.ch.mnr.gov.cn//index/user/myColumn?columnType=2&page=1'}

        for(i=0; i<allhref.length; i++ ){
            if(localStorage[allhref[i]]!='done'){ gotoherf = allhref[i]}
        }
        window.location.href = gotoherf
    }

    if (loc_ulr.indexOf('http://rsedu.ch.mnr.gov.cn//index/onlineCourseUser/class?courseId=') != -1)
    {
        var connection = new WebSocket('ws://rsedu.ch.mnr.gov.cn//websocketVideoTime');
        connection.onopen = wsOpen;
        function wsOpen(event)
        {
            var allvideo_numbers = window.videos.length;
            var all_not_learnstudy = new Array();
            var courseID
            var orderID
            var videoID
            var userID
            var end_websocket_code
            var videoTIME
            var updatetimes

            for (var i=0;i<allvideo_numbers; i++)
            {
                if (window.videos[i].IS_END != '1')
                {
                    all_not_learnstudy.push(i)

                }
            }
            if (all_not_learnstudy.length==0)
            {
                localStorage[loc_ulr]='done'
                localStorage["cehui_refresh"]=1;
                gotoherf = 'http://rsedu.ch.mnr.gov.cn//index/user/myColumn?columnType=3'
            }

            for(i=0;i<all_not_learnstudy.length;i++)
            {
            courseID = window.videos[all_not_learnstudy[i]].COURSE_ID;
            orderID = window.videos[all_not_learnstudy[i]].ORDER_ID;
            videoID = window.videos[all_not_learnstudy[i]].VIDEO_ID;
            userID = window.videos[all_not_learnstudy[i]].USER_ID;
            videoTIME= window.videos[all_not_learnstudy[i]].VIDEOS_TIME;
            updatetimes = Math.floor(videoTIME*60/10);

            end_websocket_code ='{"type":"endData","data":{"courseId":"'+courseID+'","playTime":'+(videoTIME*60).toString()+',"orderId":"'+orderID+'","allTime":'+(videoTIME*60).toString()+',"videoId":"'+videoID+'","userId":"'+userID+'"}}';
            connection.send(end_websocket_code);
            sleep(2000)
            }
            localStorage["cehui_refresh"]=1
            gotoherf = 'http://rsedu.ch.mnr.gov.cn//index/user/myColumn?columnType=3'
            window.location.href = gotoherf
        }


    }

    if (loc_ulr.indexOf('http://rsedu.ch.mnr.gov.cn//index/user/myColumn?columnType=2&page') != -1)
    {
        var a_target_number = document.getElementsByTagName('a').length;
        var study_statues;
        var all_need_to_learn = new Array();
        var class_href

        if(localStorage["cehui_refresh"]==1)
        {
            localStorage["cehui_refresh"]=0;
            location.reload(true);
        }
        if(localStorage['xuanxiu_refresh']==1)
        {
            localStorage['xuanxiu_refresh']=0;
            location.reload(true);
        }

        //读取所有未学习课程,将a标签下标放入数组all_need_to_learn
        for( i=0;i<a_target_number;i++)
        {
            if (typeof(study_statues = document.getElementsByTagName('a')[i].getElementsByTagName('span')[0]) != "undefined")
            {
                study_statues = document.getElementsByTagName('a')[i].getElementsByTagName('span')[0].innerHTML
                if (study_statues=='正在学习'||study_statues=='未学习')
                {
                    all_need_to_learn.push(i)
                }
            }

        }

        //如果没找到没学的，就不学了
        if (all_need_to_learn.length==0)
        {
            localStorage[loc_ulr]='done'
            localStorage["cehui_refresh"]=1;
            window.location.href = 'http://rsedu.ch.mnr.gov.cn//index/user/myColumn?columnType=3'
        }

        //开始学习
        for(i=0;i<all_need_to_learn.length;i++)
        {
            class_href = document.getElementsByTagName('a')[all_need_to_learn[i]].getAttribute('href')
            gotoherf=class_href
            window.location.href = gotoherf
        }
    }

    if (loc_ulr.indexOf('http://rsedu.ch.mnr.gov.cn//index/play?courseId=')!= -1)
    {
        function post(URL, PARAMS)
        {
            var temp = document.createElement("form");
            temp.action = URL;
            temp.method = "post";
            temp.style.display = "none";
            for (var x in PARAMS) {
                var opt = document.createElement("textarea");
                opt.name = x;
                opt.value = PARAMS[x];
                temp.appendChild(opt);
            }
            document.body.appendChild(temp);
            temp.submit();
            return temp;
        }
        var courseIDs
        courseIDs = window.courseId
        post('http://rsedu.ch.mnr.gov.cn//index/course/addCorn',{courseId:courseIDs})
    }
}
)();